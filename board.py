import random, copy

#This class will construct an NxN board given an integer N > 0

class Board:
    
    #build an NxN board given the size N
    def __init__(self, N):
        self.grid=[[0 for x in range(N)] for x in range(N)]
        self.size=N
        self.numSet=[x+1 for x in range(N**2)] #1,2,...,N^2
        self.spaceLocation=(-1,-1)
        self.priority=-1
        self.prevBoard=None
        self.moves=0
    
    #set the priority of the board
    def setPriority(self):
        self.priority=self.manhatten()+self.moves
        return self.priority
    
    #create a random board
    def generateBoard(self):
        for i in range(self.size):
            for j in range(self.size):
                val=random.sample(self.numSet, 1)
                val=val[0]
                self.grid[i][j]=val%9 #the "0" will denote the empty space

                #keep track of where the empty space is
                if val%9 ==0:
                    self.spaceLocation=(i,j)
                    #print self.spaceLocation #OK
                    
                self.numSet.remove(val) #removed all values in numSet!

        #set the priority
        self.priority=self.setPriority()
                
        return self.grid

    #create a board given a list of numbers (numList =0,...,8)
    def readInBoard(self, numList):
        k=0
        for i in range(self.size):
            for j in range(self.size):
                self.grid[i][j]=numList[k]
                
                #keep track of where the empty space is
                if numList[k]%9 ==0:
                    self.spaceLocation=(i,j)
                    #print self.spaceLocation #OK

                k=k+1
                
        #set the priority
        self.priority=self.setPriority()
        
    #print out this board
    def displayBoard(self):
        for i in range(self.size):
            for j in range(self.size):
                print self.grid[i][j],
            print '\n'

    #get board corresponding to the case when sliding value to the left
    #into the space
    def getLeftBoard(self):
        if self.spaceLocation[1] < self.size-1:

            #create a new instance of a board
            leftBoard=Board(self.size)
            leftBoard.grid=copy.deepcopy(self.grid)
            leftBoard.numSet = [x+1 for x in range(self.size**2)]
            leftBoard.spaceLocation=(self.spaceLocation[0],self.spaceLocation[1]+1)

            #slide to the left
            leftBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]] = \
            leftBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]+1]

            #zero out the appropriate spot
            leftBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]+1] = 0

            #set the prevBoard
            leftBoard.prevBoard=self

            #set the number of moves
            leftBoard.moves=self.moves+1

            #set the priority
            leftBoard.priority=leftBoard.setPriority()

            return leftBoard

    #get board corresponding to the case when sliding value to the right
    #into the space
    def getRightBoard(self):
        if self.spaceLocation[1] > 0:

            #create a new instance of a board
            rightBoard=Board(self.size)
            rightBoard.grid=copy.deepcopy(self.grid)
            rightBoard.numSet = [x+1 for x in range(self.size**2)]
            rightBoard.spaceLocation=(self.spaceLocation[0],self.spaceLocation[1]-1)

            #slide to the left
            rightBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]] = \
            rightBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]-1]

            #zero out the appropriate spot
            rightBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]-1] = 0

            #set the prevBoard
            rightBoard.prevBoard=self

            #set the number of moves
            rightBoard.moves=self.moves+1

            #set the priority
            rightBoard.priority=rightBoard.setPriority()

            return rightBoard

    #get board corresponding to the case when sliding value down
    #into the space
    def getDownBoard(self):
        if self.spaceLocation[0] > 0:

            #create a new instance of a board
            downBoard=Board(self.size)
            downBoard.grid=copy.deepcopy(self.grid)
            downBoard.numSet = [x+1 for x in range(self.size**2)]
            downBoard.spaceLocation=(self.spaceLocation[0]-1,self.spaceLocation[1])

            #slide down
            downBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]] = \
            downBoard.grid[self.spaceLocation[0]-1][self.spaceLocation[1]]

            #zero out the appropriate spot
            downBoard.grid[self.spaceLocation[0]-1][self.spaceLocation[1]] = 0

            #set the prevBoard
            downBoard.prevBoard=self

            #set the number of moves
            downBoard.moves=self.moves+1

            #set the priority
            downBoard.priority=downBoard.setPriority()

            return downBoard

    #get board corresponding to the case when sliding value up
    #into the space
    def getUpBoard(self):
        if self.spaceLocation[0] < self.size-1:

            #create a new instance of a board
            upBoard=Board(self.size)
            upBoard.grid=copy.deepcopy(self.grid)
            upBoard.numSet = [x+1 for x in range(self.size**2)]
            upBoard.spaceLocation=(self.spaceLocation[0]+1,self.spaceLocation[1])

            #slide down
            upBoard.grid[self.spaceLocation[0]][self.spaceLocation[1]] = \
            upBoard.grid[self.spaceLocation[0]+1][self.spaceLocation[1]]

            #zero out the appropriate spot
            upBoard.grid[self.spaceLocation[0]+1][self.spaceLocation[1]] = 0

            #set the prevBoard
            upBoard.prevBoard=self

            #set the number of moves
            upBoard.moves=self.moves+1

            #set the priority
            upBoard.priority=upBoard.setPriority()

            return upBoard

    #get adjacent boards
    def getAdjacentBoards(self):
        adjBoards=[]
        adjBoards.append(self.getUpBoard())
        adjBoards.append(self.getDownBoard())
        adjBoards.append(self.getLeftBoard())
        adjBoards.append(self.getRightBoard())

        return adjBoards


    #compute the manhatten distance
    def manhatten(self):
        dist=0
        solved={ 1: (0,0), 2: (0,1), 3: (0,2), 4: (1,0), 5: (1,1), 6: (1,2), \
                 7: (2,0), 8: (2,1), 9: (2,2)};
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] != 0:
                    val = self.grid[i][j]
                    trueCoords = solved[val]
                    dist=dist+abs(trueCoords[0]-i)+abs(trueCoords[1]-j)
        return dist

    #Count the number of inversions a board has.
    def countInversions(self):
        order=[]
        numInv=0

        #add numbers in grid to list
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] != 0:
                    order.append(self.grid[i][j])

        #count number of inversions
        for i in range(len(order)):
            for j in range(i+1,len(order)):
                if order[j] < order[i]:
                    numInv=numInv+1  
        return numInv

    #check whether a board is solvable or not
    def isSolvable(self):
        if self.countInversions()%2 == 1:
            return False
        return True

    #Check if a given board is equal to this board
    def equals(self, otherBoard):
        for i in range(self.size):
            for j in range(self.size):
                if self.grid[i][j] != otherBoard.grid[i][j]:
                    return False
        return True

    #Check if the is board is the goal board
    def isSolution(self):
        return self.manhatten()==0







