import random, copy, board, Queue

#Given a board, the Solver class will check if it is solvable. If it is solvable, then it will determine the number of 
#moves needed to reach the goal board from the initial board and print the intermediate boards required to reach the goal board.

class Solver:

    #Use the A* algorithm to find a solution
    def __init__(self, initialBoard):
        self.Board = initialBoard
        self.iterations=0
        self.numMoves=0;
        self.Goal=None

        #If the board is solvable
        if self.Board.isSolvable() == True:
            
            #Create the priority queue
            q=Queue.PriorityQueue()
            q.put((self.Board.priority, self.Board))
            
            while(not q.empty()):
                r = q.get()

                theBoard=r[1]
                
                #if we have not yet found the solution, we proceed with BFS
                if not theBoard.isSolution():

                    #add to counter
                    self.iterations=self.iterations+1
                    
                    for boards in theBoard.getAdjacentBoards():
                        if boards != None and boards != theBoard.prevBoard:
                            q.put((boards.priority, boards))
                else:
                    #"Solution has been reached"
                    self.numMoves=theBoard.moves
                    self.Goal=theBoard
                    break

        else:
            print "board is not solvable"
    
    #return the number of moves it took to reach the solution from the initial board
    def moves(self):
        return self.numMoves

    #print out a sequence of boards that will lead to goal board from the initial board
    def solnSequence(self):
        sequence=[]
        sequence.append(self.Goal)
        cur=self.Goal
        while cur.prevBoard != None:
            sequence.append(cur.prevBoard)
            cur=cur.prevBoard
        for j in range(len(sequence)-1,-1,-1):
            sequence[j].displayBoard()
            print "----------------"






            
