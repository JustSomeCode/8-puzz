import pytest
import board, Solver
    
#this board [1,2,3,0,7,6,5,4,8] requires 7 moves to reach the solution [1,2,3,4,5,6,7,8,0]
def test_7moves():
    b=board.Board(3)
    b.readInBoard([1,2,3,0,7,6,5,4,8])
    s=Solver.Solver(b)
    s.solnSequence()
    assert s.moves() == 7

test_7moves()
